/**
 * Copyright (c) 2014 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.accsoft.steering.jmad.modeldefs.defs.ad;

import java.util.HashSet;
import java.util.Set;

import cern.accsoft.steering.jmad.MadXConstants;
import cern.accsoft.steering.jmad.domain.beam.Beam;
import cern.accsoft.steering.jmad.domain.file.CallableModelFileImpl;
import cern.accsoft.steering.jmad.domain.file.ModelPathOffsets;
import cern.accsoft.steering.jmad.domain.file.ModelPathOffsetsImpl;
import cern.accsoft.steering.jmad.domain.file.CallableModelFile.ParseType;
import cern.accsoft.steering.jmad.domain.file.ModelFile.ModelFileLocation;
import cern.accsoft.steering.jmad.domain.machine.RangeDefinitionImpl;
import cern.accsoft.steering.jmad.domain.machine.SequenceDefinitionImpl;
import cern.accsoft.steering.jmad.domain.twiss.TwissInitialConditionsImpl;
import cern.accsoft.steering.jmad.modeldefs.ModelDefinitionFactory;
import cern.accsoft.steering.jmad.modeldefs.domain.JMadModelDefinition;
import cern.accsoft.steering.jmad.modeldefs.domain.JMadModelDefinitionImpl;
import cern.accsoft.steering.jmad.modeldefs.domain.OpticsDefinition;
import cern.accsoft.steering.jmad.modeldefs.domain.OpticsDefinitionImpl;

public class AdModelDefinitionFactory implements ModelDefinitionFactory {
    
private void addInitFiles(JMadModelDefinitionImpl modelDefinition) {
    
    /*
     * call the two strength definition files AD
     */
    modelDefinition.addInitFile(new CallableModelFileImpl(
            "strength/ad_quads_3837_ffe.str",ModelFileLocation.REPOSITORY));
    

    /* elements */
    modelDefinition.addInitFile(new CallableModelFileImpl("elements/ad.ele",ModelFileLocation.REPOSITORY));

    /*
     * call the aperture definition file for AD. 
     */
    modelDefinition.addInitFile(new CallableModelFileImpl(
            "aperture/ad.dbx",ModelFileLocation.REPOSITORY));

    /*
     * call the sequence definition file for AD
     */
    modelDefinition.addInitFile(new CallableModelFileImpl("sequence/ad.seq",ModelFileLocation.REPOSITORY));

    /* beam */
    modelDefinition.addInitFile(new CallableModelFileImpl(
            "beams/adPositron.beamx", ModelFileLocation.REPOSITORY));
    
   
    
    
    
    
   
    
}

private ModelPathOffsets createModelPathOffsets() {
    ModelPathOffsetsImpl offsets = new ModelPathOffsetsImpl();
    offsets.setResourceOffset("");
    offsets.setRepositoryOffset("2018");
    return offsets;
}

private Set<OpticsDefinition> createOpticsDefinitions(){
    Set<OpticsDefinition> definitionSet = new HashSet<>();
    definitionSet.add( new OpticsDefinitionImpl(
            "Nominal-2018", new CallableModelFileImpl(
                    "strength/ad_quads_3837_ffe.str",
                    ModelFileLocation.REPOSITORY, ParseType.STRENGTHS)));
    return definitionSet;
}

    @Override
    public JMadModelDefinition create() {
        JMadModelDefinitionImpl modelDefinition = new JMadModelDefinitionImpl();
        
        modelDefinition.setName("AD");
        modelDefinition.setModelPathOffsets(createModelPathOffsets());
        

        this.addInitFiles(modelDefinition);
        
        for(OpticsDefinition opticsDefinition :  createOpticsDefinitions()) {
        modelDefinition.addOpticsDefinition(opticsDefinition);
        if(opticsDefinition.getName()=="Nominal-2018") 
            modelDefinition.setDefaultOpticsDefinition(opticsDefinition);
        }
        
        /*
         * SEQUENCE
         */
        SequenceDefinitionImpl ad = new SequenceDefinitionImpl("ad", createBeam());
        modelDefinition.setDefaultSequenceDefinition(ad);

        TwissInitialConditionsImpl twiss = new TwissInitialConditionsImpl(
                "default-twiss");
        twiss.setCalcAtCenter(true);
        twiss.setCalcChromaticFunctions(true);
        ad.setDefaultRangeDefinition(new RangeDefinitionImpl(ad, "ALL",
                        twiss));

        return modelDefinition;
    }
    
    public static Beam createBeam() {
        Double energy = 20.0; // energy in GeV
        Double gamma = energy / MadXConstants.MASS_PROTON; // beta
        Double emittance = 3.5e-06; // normalized emittance
        Double xEmittance = emittance / (gamma);
        Double yEmittance = emittance / (gamma);

        Beam beam = new Beam();
        beam.setParticle(Beam.Particle.PROTON);
        beam.setEnergy(energy);
        beam.setBunchLength(0.077);
        beam.setDirection(Beam.Direction.PLUS);
        beam.setParticleNumber(1.1E11);
        beam.setRelativeEnergySpread(5e-4);
        beam.setHorizontalEmittance(xEmittance);
        beam.setVerticalEmittance(yEmittance);
        return beam;
    }
}

