package cern.accsoft.steering.jmad.factory;

import cern.accsoft.steering.jmad.MadXConstants;
import cern.accsoft.steering.jmad.domain.beam.Beam;
import cern.accsoft.steering.jmad.domain.beam.Beam.Particle;

public final class BeamFactory {

    private BeamFactory() {
        // only static methods
    }

    /**
     * creates the beam which can be used by default for AD
     * 
     * @return the beam
     * Beam, particle=POSITRON, MASS=0.51099906E-3, ENERGY=1.0,PC=0.99999986944, GAMMA=1.956950762297E3;
     * 
     */
    public static Beam createDefaultAdBeam() {
        Double energy = 1.0; // energy in GeV
        Double gamma = 1.956950762297E3; // beta
        Beam beam = new Beam();
        beam.setParticle(Beam.Particle.POSITRON);
        beam.setEnergy(energy);
        beam.setBunchLength(0.077);
        beam.setDirection(Beam.Direction.PLUS);
        beam.setParticleNumber(1.1E11);
       
        return beam;
    }

    
}
